\section{Exercise 3}
\subsection{Design a \textsc{Datalog}-like database
(extensional plus intensional parts) with relations for
storing information about observed objects including
speed (slow, moderate, high), type (car, truck,
motorbike, bicycle, ...) etc., containing, among others,
information about connected objects.}

Our database will be backed by the many-valued logic of Łukasiewicz and Tarski.
That is, the semantics of $\land, \lor, \lnot$ are:
\begin{equation}
    e(A \land B) = min[e(A), e(B)]
\end{equation}
\begin{equation}
    e(A \lor B) = max[e(A), e(B)]
\end{equation}
\begin{equation}
    e(\lnot A) = 1 - e(A)
\end{equation}
Facts are known with uncertainty, that is queries with zero free variables
do not evaluate to $\mathfrak{t}, \mathfrak{f}$ but to any value in range
$[0...1]$.


\noindent
Facts:\\
\begin{itemize}
    \item About object's speed:\\
    $highSpeed(O)$\\
    $moderateSpeed(O)$\\
    $slowSpeed(O)$
    \item About object's appearance:
    $occupiesEntireLane(O)$\\
    $isTall(O)$\\
    $hasFrontLights(O, (0|1|2))$\\
    $hasRearLights(O, (0|1|2))$
    \item Object's type (facts determined from deductive rules)\\
    $isCar(O)$\\
    $isTruck(O)$\\
    $isMotorbike(O)$\\
    $isBicycle(O)$
    \item Connection:\\
    $directlyConnected(O_1, O_2)$
\end{itemize}

\noindent
Formulas:\\
$\forall_O \exists_{\alpha, \gamma, \rho}
\big[
\alpha + \gamma + \rho = 1 \land 
highSpeed(O) = \alpha \land moderateSpeed(O) = \gamma \land
slowSpeed(O) = \rho \big]$\\
$\forall_O \exists_{\alpha, \gamma, \rho, \phi}
\big[
\alpha + \gamma + \rho + \phi = 1 \land 
isCar(O) = \alpha \land isTruck(O) = \gamma \land
isMotorbike(O) = \rho \land isBicycle(O) = \phi
\big]$\\


\subsection{Provide sample default rules for classifying an object to be a car.}
Rules:\\
$hasLightsLikeACar(O) :- hasFrontLights(O, 2)$\\
$hasLightsLikeACar(O) :- hasRearLights(O, 2)$\\
$isCar(O) :- hasLightsLikeACar(O), \lnot isTall(O), occupiesEntireLane(O)$\\
$connected(O_1, O_2, 1) :- directlyConnected(O_1, O_2)$\\
$connected(O_1, O_2, n) :- directlyConnected(O_1, O_3, n-1), directlyConnected(O_3, O_2, 1)$\\
$connected(O_1, O_2) :- connected(O_1, O_2, n)$\\

\subsection{Formulate queries}
In a two-valued logic, a result of a query is a set of all tuples
of free variables values such that the query evaulates to $\mathfrak{t}$. In
the many-valued logic we're using here, when executing a query, we should
also specify the designated truth values (for instance, $>0.8$).

From a practical point of view, it seems more convenient to allow choosing
designated truth values per each query (instead of using the same value
for all queries).
\subsubsection{selecting all objects moving with a slow speed connected to a
car}
$slowSpeed(X), connected(X, Y), isCar(Y)$
\subsubsection{selecting all objects which are directly or indirectly connected
to a given object, where we say that an object $Q$ is indirectly connected to
an object $P$ if there is a natural number $n \geq 1$ and objects
$O_1, ..., O_n$ such that $O_1 = P, O_n = Q$ and for all $1 < i \leq n$,
$O_i$ is connected to $O_{i-1}$}
$connected(P, Q)$ (see the rule definition for $connected$ above)

\subsection{Please discuss how logics designed in Exercises 1 and 2 could be
used in queries}
The logic from Exercise 1 could be used instead of Łukasiewicz-Tarski logic
here, but the $\mathfrak{u}$ (uncertain) truth value would be less meaningful
than a probability in range $[0..1]$. On the other hand, the truth value
$\mathfrak{in}$ (indefinite) might come in handy to represent unknown data
(real life SQL databases are often sparse and contain a lot of $\mathfrak{null}$
values).

An example use case for the modal logic from exercise 2 -- consider a query:
\begin{equation}
    isCar(X) \lor \mathbb{U} isCar(X)
\end{equation}
Recall that the logic from ex. 2 is two-valued and that it reduces the world
function image from $[0..1]$ to $\mathfrak{t}, \mathfrak{f}$. That is, object
$X$ may satisfy the $isCar(X)$ formula, but not be a car (although, under
current knowledge of the world, it's a car with probability $0.6$). On the
contrary, object $X$ may not satisfy the formula $isCar(X)$, but be a car.
The query presented above should be interpreted as:
\begin{equation}
    X \text{ either is certainly a car or may be a car}
\end{equation}
in other words:
\begin{equation}
    \text{It is not yet certain that } X \text{ is not a car}
\end{equation}
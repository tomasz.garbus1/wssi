\section{Exercise 2}
$\mathbb{U}(A) \overset{def}{\equiv}$ "$A$ is uncertain".\\

\subsection{Describe informally the intended semantical
structures (relating your solution to a particular
chosen real-world context, in particular interpret worlds
from $\mathcal{W}$)}

Let's start with an example interpretation of worlds which will motivate
the logic's design.

Let's say we are training an AI agent, eg. to detect credit
frauds.
The agent is trained by showing it samples of fraudulent or non-fraudulent credit
card users and labels (\textit{"this person is a scammer a.k.a. 1"}, \textit{"this person is honest a.k.a. 0"}).
When asked about those credit card users again, it is certain about the answer: $0$ (honest) or $1$ (scammer).
For other people, it is never
certain -- it returns a probabilty $p$, $0 < p < 1$ that the person is fraudulent,
using for example $k$-nearest neighbors heuristic (and smoothing the output to ensure
that the probabilty is never $0$ or $1$ for previously unobserved samples).

Now, a single world represents a state of training the AI agent. Each bank client $c$ has
a propositional variable $fraud_c$. In world $w$, $w(fraud_c) = 1 \lor w(fraud_c) = 0$ if
our AI agent has already been trained on this sample and $0 < w(fraud_c) < 1$ otherwise.

A world $v$ is "later" than world $w$ is $v$ can be reached from $w$ by showing the AI agent
more samples from the labelled dataset. I propose the following definition to express this
intuition:

\begin{equation*}
    \begin{multlined}
    w \prec v \overset{def}{\equiv}
    \big[\forall_{p \in PV} (w(p) = 0.0 \Rightarrow v(p) = 0.0) \land (w(p) = 1.0 \Rightarrow v(p) = 1.0) \big]
    \land
    \big[\exists_{p \in PV} 0.0 < w(p) < 1.0 \land (v(p) = 0.0 \lor v(p) = 1.0) \big] \\
    w = v \overset{def}{\equiv} \forall_{p \in PV} w(p) = v(p) \\
    w \preceq v \overset{def}{\equiv} w = v \lor w \prec v \\
    \end{multlined}
\end{equation*}

In other words, worlds $w$ and $v$ are equivalent if and only if they evaluate each propositional
variable exactly the same. World $v$ is later than $w$ if the evaluation of data that we are certain
about does not change, but we may gain some certainties and re-evaluate probability distribution for
other variables. Note that if worlds $w$ and $v$ have exactly the same sets of variables that we
are certain about but the probability estimations for other variables differ, then worlds $w$ and $v$
are incomparable.

Of course in world $w \in \mathcal{W}$ there may be valuations of propositional variables other than
$fraud_c$, for instance input variables to the agent, such as $got\_a\_record_c$, $is\_male_c$, $is\_young_c$
(here the fuzziness is useful) etc. but in a real life scenario they are not subject to change as we
train the agent.

\subsection{Provide an (informal) example where $\mathbb{U}$
is not extensional}
Let's imagine two twins, with very similar appearrance, life stories, income etc. In other words, twins
$t1$ and $t2$ evaluate exactly the same in world $w \in \mathcal{W}$ propositional variables
such as $has\_high\_income_{t1}, has\_high\_income_{t2}$. The AI agent has already seen
twin $t1$ and knows certainly that the person is not a scammer. Even though $t1$ and $t2$ are
equivalent in all aspects expressed in propositional variables, since $t2$ is a different object,
the AI agent is not sure that $t2$ is not fraudulent.

\subsection{Formalize the structures selected in Point 4. (here point 1.)
using Kripke-like semantics}
The relevant Kripke structure for the example from point 1 (the AI agent)
is as follows:
\begin{itemize}
    \item $\mathcal{W}$ -- the set of worlds. Here we assume that the worlds are
          non-partial function, i.e. every propositional variable has some
          fuzzy valuation
    \item the relation $\mathcal{R}$ is the $\preceq$ relation from point 1
    \item $v$ is a mapping $\mathcal{F} \times \mathcal{W} \rightarrow \{ \mathfrak{t}, \mathfrak{f} \}$
          defined as follows:
          $v(f, w) = \begin{cases}
            \mathfrak{t} \ \ \ \ \text{if } w(f) > 0.5\\
            \mathfrak{f} \ \ \ \ \text{if } w(f) \leq 0.5\\
          \end{cases}$

\end{itemize}

\subsection{Decide whether $\mathbb{U}$ should be modelled
by $\Box$ or $\Diamond$}
$\mathbb(U)(A)$ is modelled by $\Diamond$, but a bit different than in base Kripke
semantics:
\begin{equation}
    \mathcal{K}, w, v \models_{\mathcal{M}} \mathbb{U}(A) \Leftrightarrow
    {\big[} \mathcal{K}, w, v \models_{\mathcal{M}} A \land \exists_{w^\prime} w \preceq w^\prime \land \mathcal{K}, w^\prime, v \models_{\mathcal{M}} \lnot A {\big]}
    \lor 
    {\big[} \mathcal{K}, w, v \models_{\mathcal{M}} \lnot A \land \exists_{w^\prime} w \preceq w^\prime \land \mathcal{K}, w^\prime, v \models_{\mathcal{M}} A {\big]}
\end{equation}
In natural language, this means: $\mathbb{U}(A)$ iff in current world $w$ we
estimate $A$ to be true, but in some later world (i.e. world with more learned facts) $w^\prime$,
$A$ is false (or vice versa).

Note that by this definition, $\mathbb{U}(A)$ if and only if $\mathbb{U}(\lnot A)$.
That seems intuitive, because if $A$ is uncertain than so is $\lnot A$.

\subsection{Give the dual modality a meaningful name in the
natural language}
Let's start by interpreting the dual modality semantics:
\begin{equation*}
    \begin{multlined}
    \mathcal{K}, w, v \models_{\mathcal{M}} \Box A \overset{def}{\equiv} \mathcal{K}, w, v \models_{\mathcal{M}} \lnot \mathbb{U} \lnot A\\
     \Leftrightarrow \\
    {\big[} \mathcal{K}, w, v \models_{\mathcal{M}} A \land \Leftrightarrow \forall_{w^\prime} w \preceq w^\prime \Rightarrow \mathcal{K}, w^\prime, v \models_{\mathcal{M}} A {\big]}
    \lor 
    {\big[} \mathcal{K}, w, v \models_{\mathcal{M}} \lnot A \land \Leftrightarrow \forall_{w^\prime} w \preceq w^\prime \Rightarrow \mathcal{K}, w^\prime, v \models_{\mathcal{M}} \lnot A {\big]}
    \end{multlined}
\end{equation*}
A clear interpretation of the dual modality emerges -- $\Box$ models "certainty",
$\mathbb{C}(A) \overset{def}{\equiv} A \text{ is certain (to be true or to be false)}$.

Note that $K, w, v \models_{\mathcal{M}} \mathbb{U}(A)$ iff $0 < w(A) < 1$
and $K, w, v \models_{\mathcal{M}} \mathbb{C}(A)$ if $w(A) \in \{0, 1\}$\footnote{
    This is an informal notation because the variable $A$ should be
    first mapped by model $\mathcal{M}$ to an object in world $w$.
}.

\subsection{Discuss whether the axioms $D, T, 4$ and $B$ are
valid under your semantics}
\subsubsection{D}
This axiom is not true, in fact it is always false. Under my semantics,
the two dual operators $\mathbb{U}$ and $\mathbb{C}$ are negations of each other.
\subsubsection{T}
This axiom is also not true, since $\mathbb{C}(A)$ means "value of $A$ is certain"
rather than "$A$ being true is certain".
\subsubsection{4}
The proposed relation $\preceq$ between worlds is transitive and reflexive and so
the axiom "4" is holds under my semantics.
\subsubsection{B}
Axiom "B" doesn't hold. Consider a Kripke structure $\mathcal{K}, w, v$, such
that $w(A) = 1$. The left side of implication $A \rightarrow \mathbb{C} \mathbb{U} A$
is true and the right side is false.